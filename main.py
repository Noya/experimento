from flask import Flask
from flask import render_template
from flask import request
from flask import redirect, flash

import RegistrationForm

app= Flask(__name__)

@app.route('/registro',methods=['GET', 'POST'])
def register():
    title = "Registro de Usuarios"
    form = RegistrationForm.RegistrationForm(request.form)
    if request.method == 'POST' and form.validate():
       # user = User(form.username.data, form.email.data,
        #            form.password.data)
       # db_session.add(user)
        flash('Thanks for registering')
       # return redirect(url_for('login.html'))
    return render_template('index.html', form=form)

if __name__ == '__main__':
    app.run(debug=True)
