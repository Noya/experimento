from wtforms import Form
from wtforms.validators import DataRequired
from wtforms import StringField, TextField, PasswordField
from wtforms.fields.html5 import EmailField
from wtforms import validators
#class CommentForm(Form):
#    username=StringField('username', [validators.DataRequired()])
#    email= EmailField('Correo Electronico', [validators.DataRequired()])
#    password = PasswordField('Password', [validators.DataRequired(), validators.length(min=8)])
#    Respuesta = TextField('Respuesta', [validators.DataRequired()])
from wtforms import Form, StringField, PasswordField, validators

class RegistrationForm(Form):
    username = StringField('Username', [validators.Length(min=4, max=25)])
    email = StringField('Email Address', [validators.Length(min=6, max=35)])
    password = PasswordField('New Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')
    Respuesta = TextField('Respuesta', [validators.DataRequired()])